
import {Form, Button} from 'react-bootstrap'
import { useEffect, useState, useContext } from 'react'
import {Navigate, Link} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'
import { useState } from 'react'
export default function Register(){

function App() {
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')



		const data = await response.json()

    function loginUser(e) {
        // Syntax:
            // fetcj('url', options)
        fetch('http://localhost:4000/users/login', {

        method : 'POST',
        headers : {
            'Content-Type' : 'application/json'
        },
        body: JSON.stringify({
            email : email,
            password : password
        })

        }).then(res => res.json())
        .then(data => {

            console.log(data)
            if(typeof data.accessToken !== "undefined"){
                localStorage.setItem('token',data.accessToken)
                retrieveUserDetails(data.accessToken)

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Booking App of 196!"
                }  
                
                )
            } else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Check your credentials"
                }) 
            }
        
        })

	return (
		<div>
			<h1>Login</h1>
			<form onSubmit={loginUser}>
				<input
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					type="email"
					placeholder="Email"
				/>
				<br />
				<input
					value={password}
					onChange={(e) => setPassword(e.target.value)}
					type="password"
					placeholder="Password"
				/>
				<br />
				<input type="submit" value="Login" />
			</form>
		</div>
	)
}

export default App