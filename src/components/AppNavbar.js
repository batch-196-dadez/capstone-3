import {useState, useContext} from 'react';
import {Link} from 'react-router-dom';
import {Navbar, Container, Nav, FormGroup, FormControl, Button} from 'react-bootstrap';
import UserContext from '../UserContext';

export default function AppNavbar(){

	const {user} = useContext(UserContext);
	return (
	<Navbar bg="light" expand="sm">
	  <Container>
	    <Navbar.Brand as={Link} to="/">Welcome</Navbar.Brand>
	     	<Navbar.Toggle aria-controls="basic-navbar-nav" />
	       		<Navbar.Collapse id="basic-navbar-nav">
	        		<Nav className="me-auto">
	         		  <Nav.Link as={Link} to="/">Home</Nav.Link>
	          			 <Nav.Link as={Link} to="/product">Product</Nav.Link>
	          			 <Navbar.Form pullLeft>
      <FormGroup>
        <FormControl type="text" placeholder="Search" />
      </FormGroup>{' '}
      <Button type="submit">Submit</Button>
    </Navbar.Form>
	         </Nav>
	       </Navbar.Collapse>
	   </Container>
	</Navbar>

	)	
};
